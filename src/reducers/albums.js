import { combineReducers }  from 'redux';

import types from './types';

function isLoading(state = false, action) {
    switch (action.type) {
        case types.REQUEST_ALBUMS:
            return true;
        case types.RECEIVE_ALBUMS:
            return false;
        default:
            return state;
    }
}

function list(state = [], action) {
    switch (action.type) {
        case types.RECEIVE_ALBUMS:
            return action.albums;
        default:
            return state;
    }
}

function selected(state = null, action) {
    switch (action.type) {
        case types.ALBUM_SELECTED:
            return action.album;
        default:
            return state;
    }
}

export default combineReducers({
    isLoading,
    list,
    selected
});