import API from "../common/globals";
import types from "../reducers/types";

function requestAlbums() {
    return {
        type: types.REQUEST_ALBUMS,
    }
}

function receiveAlbums(albums) {
    return {
        type: types.RECEIVE_ALBUMS,
        albums
    }
}

export function fetchAlbums() {
    return (dispatch) => {
        dispatch(requestAlbums());

        return fetch(`${API.albums}`)
            .then((response) => {
                if (!response.ok) {
                    throw { status: response.status };
                }

                return response.json();
            })
            .then((data) => {
                dispatch(receiveAlbums(data));
            })
            .catch((error) => {
                console.log(error);
            })
    }
}