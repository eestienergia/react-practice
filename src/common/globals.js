const API_URL = 'https://jsonplaceholder.typicode.com';

const API = {
    albums: `${API_URL}/albums`
};

export default API;