import React, { Component } from 'react';

class SearchBar extends Component {
    state = {
        term: '',
    };

    onInputChange = (term) => {
        this.setState({ term });
        this.props.onSearchTermChange(term);
    };

    render() {
        const { term }  = this.state;
        return(
            <div className="input-group mb-2">
                <input
                    type="text"
                    value={term}
                    className="form-control"
                    onChange={event => this.onInputChange(event.target.value)}
                    placeholder="Search albums..."
                />
            </div>
        );
    }
}

export default SearchBar;