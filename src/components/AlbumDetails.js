import React from 'react';

const AlbumDetails = ({
    selectedAlbum: { image, url, title, description, artist }
    }) => (
    <div className="card">
        <img src={image} alt={title} />
        <div className="card-body">
            <h2 className="card-title">{title}</h2>
            <h3 className="text-muted">{artist}</h3>
            <p className="card-text">{description}</p>
            <a href={url} target="_blank" className="btn btn-primary">
                Buy album
            </a>
        </div>
    </div>
);

export default AlbumDetails;