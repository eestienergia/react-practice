import React from 'react';
import AlbumListItem from './AlbumListItem';

const AlbumList = ({ albums, selectedAlbum, onAlbumClick }) => (
    <div className="list-group fixed-height">
        {albums.map(album => {
            return (
                <AlbumListItem
                    album={album}
                    key={album.id}
                    selectedAlbum={selectedAlbum}
                    onAlbumClick={onAlbumClick}
                />
            );
        })}
    </div>  
);

export default AlbumList;