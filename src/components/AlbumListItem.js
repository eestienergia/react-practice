import React from 'react';
import classes from 'classnames';

const AlbumListItem = (props) => {
    const {
        album,
        selectedAlbum,
        onAlbumClick,
    } = props;

    return(
        <div
            onClick={() => onAlbumClick(album)}
            className={classes('list-group-item list-group-item-action flex-colum align-items-start', {
                active: album.id === selectedAlbum.id
            })}
        >
            <div className="d-flex align-items-center">
                <img src={album.thumbnail_image} alt={album.title} width="100" />
                <ul className="album-metainfo-list">
                    <li className="album-metainfo-list__item album-title">
                        {album.title}
                    </li>
                    <li className="album-metainfo-list__item album-artist">
                        {album.artist}
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default AlbumListItem;