import React from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchAlbums } from "../actions";
import AlbumList from '../components/AlbumList';
import AlbumDetails from '../components/AlbumDetails';
import SearchBar from '../components/SearchBar';
import albumData from '../data.json';
import '../scss/app.scss';

class App extends React.Component {
  state = {
    albums: albumData,
    selectedAlbum: albumData[0],
    term: '',
  };

  componentDidMount() {
    // This will make API call to our rest endpoint
    // this.props.fetchAlbums();
  }

  onAlbumClick = (album) => {
    this.setState({ selectedAlbum: album });
  };

  onSearchTermChange = (term) => {
    this.setState({ term });
  };

  render() {
    const { selectedAlbum, term, albums } = this.state;
    // const { albums }  = this.props;
    const filteredAlbums = albums.filter(
      album => album.title.toLowerCase().indexOf(term.toLowerCase()) >= 0
    );
    return (
      <div className="wrapper">
        <div className="row">
          <div className="col-sm-12">
            <h1 className="text-center">Music Albums</h1>
          </div>
        </div>
        <div className="row main__content">
          <div className="col-sm-6">
            <AlbumDetails
              selectedAlbum={selectedAlbum}
            />
          </div>
          <div className="col-sm-6">
            <SearchBar
              onSearchTermChange={term => this.onSearchTermChange(term)}
            />
            <AlbumList
              albums={filteredAlbums}
              selectedAlbum={selectedAlbum}
              onAlbumClick={album => this.onAlbumClick(album)}
            />
          </div>
        </div> 
      </div>
    );
  }
};

function mapStateToProps(state) {
  return {
      albums: state.albums.list,
      selectedAlbum: state.albums.selected
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    fetchAlbums: fetchAlbums,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);