# React Training

## Requirements

### Make sure you have Node installed. If not then you can do it here: [Node](https://nodejs.org/en/download)

### Make sure you have git installed. If not then you can do it here: [Git](https://git-scm.com/downloads)

### Make sure you have any code editor installed. If not then you can use for example: [Visual Studio Code](https://code.visualstudio.com/)

### Clone the project

```sh
cd /your-path && git clone https://bitbucket.org/eestienergia/react-practice.git
```

### Run to install all the required dependencies

```sh
npm install
```

### In the project directory, you can run:

```sh
npm run start
```

Runs the app in the development mode.
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.